import os
import sys
import torch
import torchaudio
import torch.utils.data as data
import glob
sys.path.append('../datautils/')
import wavegenerator as gen
import filter
import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt
import visualizationutils
import numpy as np

dataset_root = os.path.join(os.path.dirname(os.path.dirname(os.getcwd())), 'data')


class SineTestDataset(data.Dataset):

    def __init__(self, dataset_root=dataset_root):
        self.sine_root = os.path.join(dataset_root, 'sine/test')
        if os.path.exists(os.path.join(self.sine_root, '.DS_Store')):
            os.remove(os.path.join(self.sine_root, '.DS_Store'))
        self.input_files = sorted(os.listdir(self.sine_root))

    def __getitem__(self, index):
        input_sample, fs = torchaudio.load(
            self.sine_root + '/' + self.input_files[index],
            normalization=True)
        return input_sample.reshape(-1)

    def __len__(self):
        return len(self.input_files)



class SineDataset(data.Dataset):

    def __init__(self, dataset_root=dataset_root):
        self.sine_root = os.path.join(dataset_root, 'sine')
        self.input_files = sorted(os.listdir(os.path.join(self.sine_root, 'input')))
        self.waveshaped_files = sorted(os.listdir(os.path.join(self.sine_root, 'target')))

    def __getitem__(self, index):
        input_sample, fs = torchaudio.load(
            self.sine_root + '/input/' + self.input_files[index],
            normalization=True)
        output_sample, fs = torchaudio.load(
            self.sine_root + '/target/' + self.waveshaped_files[index],
            normalization=True)
        return input_sample.reshape(-1), output_sample.reshape(-1)

    def __len__(self):
        return len(self.input_files)



class WhiteNoiseDataset(data.Dataset):
    """
    Constraints:
    input_hop   <= output_size
    input_hop   == output_hop
    output_size <= input_size
    """
    def __init__(self, input_window_size, input_hop, output_window_size, output_hop, dataset_root=dataset_root):
        self.input_window_size = input_window_size
        self.input_hop = input_hop
        self.output_window_size = output_window_size
        self.output_hop = output_hop

        whitenoise_root = os.path.join(dataset_root, 'whitenoise', 'train')
        input_path = os.path.join(whitenoise_root, 'input')
        target_path = os.path.join(whitenoise_root, 'target')

        if os.path.exists(os.path.join(input_path, '.DS_Store')):
            os.remove(os.path.join(input_path, '.DS_Store'))
        if os.path.exists(os.path.join(target_path, '.DS_Store')):
            os.remove(os.path.join(target_path, '.DS_Store'))

        self.input_files = glob.glob(os.path.join(input_path, '*.wav'))
        self.output_files = glob.glob(os.path.join(target_path, '*.wav'))

        self.input_sample, fs = torchaudio.load(self.input_files[0], normalization=True)
        self.output_sample, fs = torchaudio.load(self.output_files[0], normalization=True)

        # Stereo to mono (use left channel)
        if self.input_sample.size(1) > 1:
            self.input_sample = self.input_sample[:, :1]
        if self.output_sample.size(1) > 1:
            self.output_sample = self.output_sample[:, :1]

        #self.input_sample *= 2
        #self.output_sample *= 2
        #print(max(self.output_sample))
        #print(min(self.output_sample))

    def __getitem__(self, index):
        return self.input_sample.narrow(dim=0,
                                        start=index*self.input_hop,
                                        length=self.input_window_size).squeeze(1), \
               self.output_sample.narrow(dim=0,
                                         start=index*self.output_hop + self.input_window_size-self.output_window_size,  # Adding input window size gives ith sample based on a block of input samples (of size input_window_size) before (till) i. eg 10th outsample is based on first 10 input samples. (Causal System).
                                         length=self.output_window_size).squeeze(1)

    def __len__(self):
        return int((len(self.input_sample) - self.input_window_size) / self.input_hop) + 1



class TestDataset(data.Dataset):

    def __init__(self, file_id=0, input_window_size=100, input_hop=1, output_window_size=1, output_hop=1, file_format='wav', dataset_root=dataset_root):
        self.input_window_size = input_window_size
        self.input_hop = input_hop
        self.output_window_size = output_window_size
        self.output_hop = output_hop

        test_root = os.path.join(dataset_root, 'test')
        input_path = os.path.join(test_root, 'input')
        target_path = os.path.join(test_root, 'target')

        if os.path.exists(os.path.join(input_path, '.DS_Store')):
            os.remove(os.path.join(input_path, '.DS_Store'))
        if os.path.exists(os.path.join(target_path, '.DS_Store')):
            os.remove(os.path.join(target_path, '.DS_Store'))

        self.input_files = sorted(os.listdir(input_path))
        self.output_files = sorted(os.listdir(target_path))

        self.input_sample, fs = torchaudio.load(os.path.join(input_path, self.input_files[file_id]), normalization=True)
        if file_format == 'wav':
            self.output_sample, fs = torchaudio.load(os.path.join(target_path, self.output_files[file_id]), normalization=True)
        elif file_format == 'npy':
            self.output_sample = torch.Tensor(np.load(os.path.join(target_path, self.output_files[file_id]))).unsqueeze(1)

        # Stereo to mono (use left channel)
        if self.input_sample.size(1) > 1:
            self.input_sample = self.input_sample[:, :1]
        if self.output_sample.size(1) > 1:
            self.output_sample = self.output_sample[:, :1]

    def __getitem__(self, index):
#        return self.input_sample.narrow(0, index*self.input_hop, self.input_window_size).squeeze(1),\
#               self.output_sample.narrow(0, index * self.output_hop, self.output_window_size).squeeze(1)
        #return self.input_sample[index], self.output_sample[index][:1] # Why [:-1]? For mono?
        return self.input_sample.narrow(dim=0,
                                        start=index*self.input_hop,
                                        length=self.input_window_size).squeeze(1), \
               self.output_sample.narrow(dim=0,
                                         start=index*self.output_hop + self.input_window_size-self.output_window_size,  # Adding input window size gives ith sample based on a block of input samples (of size input_window_size) before (till) i. eg 10th outsample is based on first 10 input samples. (Causal System).
                                         length=self.output_window_size).squeeze(1)

    def __len__(self):
        return int((len(self.input_sample) - self.input_window_size) / self.input_hop) + 1


class ChirpDataset(data.Dataset):
    """
    Seperate datasets are created for train, val and test. The param 'file' denotes which one.
    Can create dateset from only one file at a time.

    Constraints:
    input_hop   <= output_size
    input_hop   == output_hop
    output_size <= input_size
    """
    def __init__(self, file, input_window_size, input_hop, output_window_size, output_hop, dataset_root=dataset_root):
        self.input_window_size = input_window_size
        self.input_hop = input_hop
        self.output_window_size = output_window_size
        self.output_hop = output_hop

        whitenoise_root = os.path.join(dataset_root, 'chirp', 'train')
        input_path = os.path.join(whitenoise_root, 'input')
        target_path = os.path.join(whitenoise_root, 'target')

        if os.path.exists(os.path.join(input_path, '.DS_Store')):
            os.remove(os.path.join(input_path, '.DS_Store'))
        if os.path.exists(os.path.join(target_path, '.DS_Store')):
            os.remove(os.path.join(target_path, '.DS_Store'))

        self.input_files = sorted(glob.glob(os.path.join(input_path, '*.wav')))
        self.output_files = sorted(glob.glob(os.path.join(target_path, '*.wav')))

        # Get index of file to be loaded
        for i, each in enumerate(self.input_files):
            if file in os.path.basename(each):
                file_index = i

        self.input_sample, fs = torchaudio.load(self.input_files[file_index], normalization=True)
        self.output_sample, fs = torchaudio.load(self.output_files[file_index], normalization=True)

        # Stereo to mono (use left channel)
        if self.input_sample.size(1) > 1:
            self.input_sample = self.input_sample[:, :1]
        if self.output_sample.size(1) > 1:
            self.output_sample = self.output_sample[:, :1]

    def __getitem__(self, index):
        return self.input_sample.narrow(dim=0,
                                        start=index*self.input_hop,
                                        length=self.input_window_size).squeeze(1), \
               self.output_sample.narrow(dim=0,
                                         start=index*self.output_hop + self.input_window_size-self.output_window_size,  # Adding input window size gives ith sample based on a block of input samples (of size input_window_size) before (till) i. eg 10th outsample is based on first 10 input samples. (Causal System).
                                         length=self.output_window_size).squeeze(1)

    def __len__(self):
        return int((len(self.input_sample) - self.input_window_size) / self.input_hop) + 1


class WhiteNoiseLiveDataset(data.Dataset):
    """
    Constraints:
    input_hop   <= output_size
    input_hop   == output_hop
    output_size <= input_size
    """
    def __init__(self, input_window_size, input_hop, output_window_size, output_hop, whitenoise_config):
        self.input_window_size = input_window_size
        self.input_hop = input_hop
        self.output_window_size = output_window_size
        self.output_hop = output_hop

        # Generate signals live
        print("Generating live data...")
        self.input_sample = gen.generateWhiteNoise(amp_in_dB=0,
                                                   low=whitenoise_config['low'],
                                                   high=whitenoise_config['high'],
                                                   mean=0,
                                                   std=1,
                                                   N=whitenoise_config['samples'],
                                                   distribution='uniform',
                                                   normalize=False)
        if whitenoise_config['filter'] == 'lowpassFirstOrder':
            self.output_sample = filter.lowpassFirstOrder(self.input_sample, decay=whitenoise_config['decay'])
        elif whitenoise_config['filter'] == 'lowpassSecondOrder':
            #self.output_sample = filter.lowpassSecondOrder(self.input_sample, fc=whitenoise_config['fc'], psi=whitenoise_config['psi'])
            self.output_sample = filter.lowpassSecondOrder(self.input_sample, f0=whitenoise_config['f0'], Q=whitenoise_config['Q'])
        elif whitenoise_config['filter'] == 'highpassSecondOrder':
            self.output_sample = filter.highpassSecondOrder(self.input_sample, f0=whitenoise_config['f0'], Q=whitenoise_config['Q'])
        elif whitenoise_config['filter'] == 'lowshelfSecondOrder':
            self.output_sample = filter.lowshelfSecondOrder(self.input_sample, f0=whitenoise_config['f0'], S=whitenoise_config['S'], gain=whitenoise_config['gain'])
        elif whitenoise_config['filter'] == 'highshelfSecondOrder':
            self.output_sample = filter.highshelfSecondOrder(self.input_sample, f0=whitenoise_config['f0'], S=whitenoise_config['S'], gain=whitenoise_config['gain'])
        elif whitenoise_config['filter'] == 'peakSecondOrder':
            self.output_sample = filter.peakSecondOrder(self.input_sample, f0=whitenoise_config['f0'], Q=whitenoise_config['Q'], gain=whitenoise_config['gain'])
        print("Data generation done.")

        # Plot signals
        if 'signals' in whitenoise_config['plot']:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            if np.max(self.input_sample) > np.max(self.output_sample):
                ax.plot(self.input_sample, label='input')
                ax.plot(self.output_sample, label='target')
            else:
                ax.plot(self.output_sample, label='target')
                ax.plot(self.input_sample, label='input')
            ax.grid()
            visualizationutils.savePlot(fig,'input_target')
            #plt.show()
            plt.close()
        if 'filter' in whitenoise_config['plot']:
            visualizationutils.plotFrequencyResponse(self.input_sample, self.output_sample, self.output_sample)


        self.input_sample = torch.Tensor(self.input_sample).unsqueeze(1)
        self.output_sample = torch.Tensor(self.output_sample).unsqueeze(1)

        # Stereo to mono (use left channel)
        if self.input_sample.size(1) > 1:
            self.input_sample = self.input_sample[:, :1]
        if self.output_sample.size(1) > 1:
            self.output_sample = self.output_sample[:, :1]

    def __getitem__(self, index):
        return self.input_sample.narrow(dim=0,
                                        start=index*self.input_hop,
                                        length=self.input_window_size).squeeze(1), \
               self.output_sample.narrow(dim=0,
                                         start=index*self.output_hop + self.input_window_size-self.output_window_size,  # Adding input window size gives ith sample based on a block of input samples (of size input_window_size) before (till) i. eg 10th outsample is based on first 10 input samples. (Causal System).
                                         length=self.output_window_size).squeeze(1)

    def __len__(self):
        return int((len(self.input_sample) - self.input_window_size) / self.input_hop) + 1

