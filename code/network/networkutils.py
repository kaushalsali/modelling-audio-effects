import sys
import os

import torch
import torch.nn as nn
from torch.autograd import Variable
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

import visualizationutils
sys.path.append('../datautils/')
import visualizationutils
import pickle




def train(model, data_loader, loss_criterion, optimizer, visualize=None, visualize_interval=1, epoch=None, save_folder='', cuda=False):

    model.train()
    total_loss = 0
    input_cache = torch.Tensor([])
    prediction_cache = torch.Tensor([])
    target_cache = torch.Tensor([])
    hop = data_loader.dataset.input_hop
    for i, batch in enumerate(data_loader):
        #print("Processing batch " + str(i))
        input, target = Variable(batch[0]), Variable(batch[1])
        if cuda and torch.cuda.is_available():
            input = input.cuda()
            target = target.cuda()

        predictions = model(input)

        # print(input.shape)
        # print(target.shape)
        # print(predictions.shape)
        # print('--------')

        if visualize is not None:
            if 'epoch' in visualize:  # Plot only first signal of first batch in a epoch
                visualizationutils.visualizeSamples(batch, predictions, num_samples=1)
                visualize = None
            elif 'batch' in visualize:  # Plot only first signal from the batch
                print("Batch " + str(i))
                visualizationutils.visualizeSamples(batch, predictions, num_samples=1)
            elif 'sample' in visualize:  # Plot every signal in the batch
                print("Batch " + str(i))
                visualizationutils.visualizeSamples(batch, predictions)
            elif 'function' in visualize or 'filter' in visualize:
                '''
                In narrow, use start=input.size(1)-hop if 0 to i samples of input correspond to ith sample of output.
                In narrow, use start=0 if 0 to i samples of input correspond to 0th sample of output.
                '''
                input_cache = torch.cat((input_cache, input.narrow(dim=1, start=input.size(1)-hop, length=hop).cpu().contiguous().view(-1)))  # start=input.size(1)-hop takes end samples of input
                prediction_cache = torch.cat((prediction_cache, predictions.narrow(dim=1, start=predictions.size(1)-hop, length=hop).cpu().contiguous().view(-1)))
                target_cache = torch.cat((target_cache, target.narrow(dim=1, start=target.size(1)-hop, length=hop).cpu().contiguous().view(-1)))

        #print(predictions.shape, predictions[0])
        #print(target.shape, target[0])

        loss = loss_criterion(predictions, target)
        #print('Loss: ', loss.item())
        total_loss += loss.item()
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    avg_loss = total_loss / len(data_loader)

    if visualize is not None:
        if epoch is not None and epoch % visualize_interval == 0:
            if 'function' in visualize:
                visualizationutils.plotWaveshaperFunction(input_cache.numpy(), prediction_cache.detach().numpy(), target_cache.numpy(), epoch=epoch, save_folder=save_folder)
            if 'filter' in visualize:
                visualizationutils.plotFrequencyResponse(input_cache.numpy(), prediction_cache.detach().numpy(), target_cache.numpy(), epoch=epoch, save_folder=save_folder)
            if 'spectrogram' in visualize:
                visualizationutils.plotSpectrogram(input_cache.numpy(), prediction_cache.detach().numpy(), target_cache.numpy(), epoch=epoch, save_folder=save_folder)
            if 'weights' in visualize:
                visualizationutils.visualizeWeights(model, epoch=epoch, save_folder=save_folder)
    return avg_loss



def evaluate(model, data_loader, loss_criterion, visualize=None, visualize_interval=1, epoch=None, save_folder='', cuda=False):
    # Visualization level  can be controlled by the 'visualize' parameter.
    # 0 : No visualization
    # 1 : Only first signal in a batch will be plotted
    # 2 : All signals in all batches will be plotted

    model.eval()
    total_loss = 0
    input_cache = torch.Tensor([])
    prediction_cache = torch.Tensor([])
    target_cache = torch.Tensor([])
    hop = data_loader.dataset.input_hop
    for i, batch in enumerate(data_loader):
        #print("Processing batch " + str(i))
        input, target = Variable(batch[0]), Variable(batch[1])
        if cuda and torch.cuda.is_available():
            input = input.cuda()
            target = target.cuda()

        predictions = model(input)

        if visualize is not None:
            if 'sample' in visualize:  # Plot every signal in the batch
                print("Batch " + str(i))
                visualizationutils.visualizeSamples(batch, predictions)
            elif 'batch' in visualize:  # Plot only first signal from the batch
                print("Batch " + str(i))
                visualizationutils.visualizeSamples(batch, predictions, num_samples=1)
            elif 'function' in visualize or 'filter'in visualize:
                input_cache = torch.cat((input_cache, input.narrow(dim=1, start=input.size(1)-hop, length=hop).cpu().contiguous().view(-1)))  # start=input.size(1)-hop takes end samples of input
                prediction_cache = torch.cat((prediction_cache, predictions.narrow(dim=1, start=predictions.size(1)-hop, length=hop).cpu().contiguous().view(-1)))
                target_cache = torch.cat((target_cache, target.narrow(dim=1, start=target.size(1)-hop, length=hop).cpu().contiguous().view(-1)))

        loss = loss_criterion(predictions, target)
        total_loss += loss.item()
    avg_loss = total_loss / len(data_loader)

    if visualize is not None: #TODO: make into one if statement
        if (epoch == 'test') or (epoch is not None and epoch % visualize_interval == 0):
            if 'function' in visualize:
                visualizationutils.plotWaveshaperFunction(input_cache.numpy(), prediction_cache.detach().numpy(), target_cache.numpy(), epoch=epoch, save_folder=save_folder)
            if 'filter' in visualize:
                visualizationutils.plotFrequencyResponse(input_cache.numpy(), prediction_cache.detach().numpy(), target_cache.numpy(), epoch=epoch, save_folder=save_folder)
            if 'spectrogram' in visualize:
                visualizationutils.plotSpectrogram(input_cache.numpy(), prediction_cache.detach().numpy(), target_cache.numpy(), epoch=epoch, save_folder=save_folder)
            if 'weights' in visualize:
                visualizationutils.visualizeWeights(model, epoch=epoch, save_folder=save_folder)

    return avg_loss#, input_cache, prediction_cache, target_cache


def test(model, data_loader, visualize=None, epoch=None, save_folder='', cuda=False):
    # Visualization level  can be controlled by the 'visualize' parameter.
    # 0 : No visualization
    # 1 : Only first signal in a batch will be plotted
    # 2 : All signals in all batches will be plotted

    model.eval()
    mse_loss_criterion = nn.MSELoss()
    mae_loss_criterion = nn.L1Loss()
    max_mse_loss = 0
    max_mae_loss = 0
    total_mse_loss = 0
    total_mae_loss = 0
    input_cache = torch.Tensor([])
    prediction_cache = torch.Tensor([])
    target_cache = torch.Tensor([])
    hop = data_loader.dataset.input_hop
    for i, batch in enumerate(data_loader):
        #print("Processing batch " + str(i))
        input, target = Variable(batch[0]), Variable(batch[1])
        if cuda and torch.cuda.is_available():
            input = input.cuda()
            target = target.cuda()

        predictions = model(input)

        if visualize is not None:
            if 'sample' in visualize:  # Plot every signal in the batch
                print("Batch " + str(i))
                visualizationutils.visualizeSamples(batch, predictions)
            elif 'batch' in visualize:  # Plot only first signal from the batch
                print("Batch " + str(i))
                visualizationutils.visualizeSamples(batch, predictions, num_samples=1)
            elif 'function' in visualize or 'filter' in visualize:
                input_cache = torch.cat((input_cache, input.narrow(dim=1, start=input.size(1)-hop, length=hop).cpu().contiguous().view(-1)))  # start=input.size(1)-hop takes end samples of input
                prediction_cache = torch.cat((prediction_cache, predictions.narrow(dim=1, start=predictions.size(1)-hop, length=hop).cpu().contiguous().view(-1)))
                target_cache = torch.cat((target_cache, target.narrow(dim=1, start=target.size(1)-hop, length=hop).cpu().contiguous().view(-1)))

        mse_loss = mse_loss_criterion(predictions, target)
        mae_loss = mae_loss_criterion(predictions, target)
        total_mse_loss += mse_loss.item()
        total_mae_loss += mae_loss.item()
        if mse_loss > max_mse_loss:
            max_mse_loss = mse_loss.item()
        if mae_loss > max_mae_loss:
            max_mae_loss = mae_loss.item()

    avg_mse_loss = total_mse_loss / len(data_loader)
    avg_mae_loss = total_mae_loss / len(data_loader)

    if visualize is not None:
        if 'function' in visualize:
            visualizationutils.plotWaveshaperFunction(input_cache.numpy(), prediction_cache.detach().numpy(), target_cache.numpy(), epoch=epoch, save_folder=save_folder)
        if 'filter' in visualize:
            visualizationutils.plotFrequencyResponse(input_cache.numpy(), prediction_cache.detach().numpy(), target_cache.numpy(), epoch=epoch, save_folder=save_folder)
        if 'spectrogram' in visualize:
            visualizationutils.plotSpectrogram(input_cache.numpy(), prediction_cache.detach().numpy(), target_cache.numpy(), epoch=epoch, save_folder=save_folder)
        if 'weights' in visualize:
            visualizationutils.visualizeWeights(model, epoch=epoch, save_folder=save_folder)

    return avg_mse_loss, max_mse_loss, avg_mae_loss, max_mae_loss, input_cache, prediction_cache, target_cache



def predict(model, data_loader):
    model.eval()
    for i, batch in enumerate(data_loader):
        input = Variable(batch[0])
        predictions = model(input)
    return predictions, input


def saveModel(model, filename, path="../../model/"):
    if not os.path.exists(path):
        os.makedirs(path)
    #path += filename + ".pt"
    path = os.path.join(path, filename + ".pt")
    torch.save(model, path)


def loadModel(model, path="../../model/"):
    path += model + ".pt"
    return torch.load(path)


def logModel(log_dict):
    col_order = ['model', 'test_loss', 'best_val_loss', 'input_size', 'hidden_size', 'epochs', 'learning_rate',
                 'learning_rate_decay_interval', 'learning_rate_decay_factor', 'momentum', 'batch_size']
    #model_no = int(sorted(glob.glob('../../model/' + log_dict['model'] + 'model*'))[-1][:-4]) + 1 # TODO Sorted model list doesn't give latest model
    new_model_name = log_dict['model'] + 'model' + str(100) + '.pt'
    #copyfile('../../model/' + log_dict['model'] + 'model.pt', '../../model/' + new_model_name)
    with open('../../model/' + log_dict['model'] + '_models.log', 'a') as logfile:
        log_dict['model'] = new_model_name[:-3]
        pd.DataFrame(log_dict, columns=col_order, index=[0]).to_csv(logfile, index=False, sep=',', header=False)


def logRun(log_dict):
    with open('../../runs/trainer.log','a') as f:
        f.write('\nNetwork Config:\n')
        for each in log_dict:
            f.write(each + ': ' + str(log_dict[each]) + '\n')
