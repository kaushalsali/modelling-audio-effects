import torch
import torch.nn as nn
import torch.nn.functional as F

class NeuralNet(nn.Module):
 
    def __init__(self, input_size, hidden_layer_size, output_layer_size):
        super(NeuralNet, self).__init__()
        self.linear_layer1 = nn.Linear(input_size, hidden_layer_size)
        self.output_layer = nn.Linear(hidden_layer_size, output_layer_size)
        
    def forward(self, audio):
        layer1_scores = self.linear_layer1(audio)
        layer1_scores = F.relu(layer1_scores)

        output_scores = self.output_layer(layer1_scores)
        return output_scores


class NeuralNetNoReLU(nn.Module):

    def __init__(self, input_size, hidden_layer_size, output_layer_size):
        super(NeuralNetNoReLU, self).__init__()
        self.linear_layer1 = nn.Linear(input_size, hidden_layer_size)
        self.output_layer = nn.Linear(hidden_layer_size, output_layer_size)

    def forward(self, audio):
        layer1_scores = self.linear_layer1(audio)
        # layer1_scores = F.relu(layer1_scores)

        output_scores = self.output_layer(layer1_scores)
        return output_scores


class NeuralNet2Layer(nn.Module):

    def __init__(self, input_size, hidden_layer_size, output_layer_size):
        super(NeuralNet2Layer, self).__init__()
        self.linear_layer1 = nn.Linear(input_size, hidden_layer_size)
        self.linear_layer2 = nn.Linear(hidden_layer_size, hidden_layer_size)
        self.output_layer = nn.Linear(hidden_layer_size, output_layer_size)

    def forward(self, audio):
        layer1_scores = self.linear_layer1(audio)
        layer1_scores = F.relu(layer1_scores)

        layer2_scores = self.linear_layer2(layer1_scores)
        layer2_scores = F.relu(layer2_scores)

        output_scores = self.output_layer(layer2_scores)
        return output_scores

