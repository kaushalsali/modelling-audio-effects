import os
import pickle
import numpy as np
from scipy.fftpack import fft
from scipy.signal import stft
from scipy.signal import freqz
from matplotlib import pyplot as plt
import matplotlib.colors as colors
import matplotlib.ticker as mtick
import gzip



def visualizeSamples(batch, predictions, num_samples=-1, plot_marker=('o', 'x')):
    batch_size = batch[1].size(0)
    if num_samples == -1:
        num_samples = batch_size
    for j in range(num_samples):
        plt.plot(batch[0][j].numpy(), marker=plot_marker[0], label='input')
        plt.plot(batch[1][j].numpy(), marker=plot_marker[1], label='target')
        plt.plot(predictions[j].detach().numpy(), marker=plot_marker[1], label='prediction')
        plt.legend(loc='upper right')
        plt.show()


def plotLoss(train_losses, val_losses, epoch=None, num_epochs_to_view=10, save=True, interactive=True):
    if interactive:
        plt.ion()
    else:
        plt.ioff()
    if epoch is None or len(train_losses) < num_epochs_to_view:
        epoch_range = np.arange(len(train_losses))
    else:
        epoch_range = np.arange(epoch-num_epochs_to_view, epoch)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel("Epoch")
    ax.set_ylabel("MSE Loss")
    ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.3e'))
    ax.plot(epoch_range, train_losses, label="train_loss")
    ax.plot(epoch_range, val_losses, label="val_loss")
    ax.legend()
    ax.grid()
    if save:
        savePlot(fig, 'loss')
    plt.show()
    plt.pause(0.0001)
    plt.close()


def plotWaveshaperFunction(input, prediction, target, save=True, epoch=None, save_folder='', interactive=True):
    #print("Max Input: ", np.max(input))
    #print("Max Target: ", np.max(target))
    #print('------')
    if interactive:
        plt.ion()
    fig = plt.figure()

    ax1 = fig.add_subplot(211)
    ax1.set_title('Absolute error between samples of input & output signal')
    ax1.plot(np.abs(prediction-target))
    #plt.plot(np.power(prediction - target, 2))
    ax1.set_xlabel("Sample number")
    ax1.set_ylabel("Absolute error")
    #ax1.set_ylim(-0.01, 0.01)
    ax1.grid()

    ax2 = fig.add_subplot(212)
    ax2.set_title('Waveshaper function')
    ax2.set_xlabel("Input sample value")
    ax2.set_ylabel("Prediction/Target value")
    #ax2.set_ylim(-1.1, 1.1)
    ax2.plot(input, target, 'x', label='Target')
    ax2.plot(input, prediction, '.', label='Prediction')
    ax2.legend()
    ax2.grid()

    if save:
        file_name = 'function_plot'
        if not epoch == None:
            file_name += '_' + str(epoch)
        save_loc = '../../runs/'
        if epoch == 'testrun':
            save_loc = '../../runs/testrun/'
        save_loc = os.path.join(save_loc, save_folder)
        savePlot(fig, name=file_name, loc=save_loc)

    plt.tight_layout()
    plt.show()
    plt.pause(0.0001)
    plt.close()
    #plt.ioff()


def plotFreqz(B, A, worN=1024):
    w, h = freqz(B, A, worN=worN)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(w*44100/(2*np.pi), toDB(np.abs(h)))
    #ax.axhline(y=-3, color='red', linewidth=0.5)
    ax.grid()
    plt.show()


def plotFrequencyResponse(input, prediction, target, fs=44100, save=True, epoch=None, save_folder='', interactive=True):

    N = 1024  # FFT Size
    n = np.arange(N)
    # print("Frequency resolution: ", fs/N)

    X = np.zeros(N)
    Y = np.zeros(N)
    Z = np.zeros(N)
    avg_size = 100  # Take avg of 100 windows (size=N) of input signal
    j = int(len(input) / avg_size)
    for i in range(avg_size):
        X += np.abs(fft(input[j*i : j*i+N], N))
        Y += np.abs(fft(prediction[j*i : j*i+N], N))
        Z += np.abs(fft(target[j*i : j*i+N], N))
    X = X / avg_size
    Y = Y / avg_size
    Z = Z / avg_size

    # if interactive:
    #     plt.ion()

    fig = plt.figure()

    ax1 = fig.add_subplot(311)
    ax1.set_title('Time domain signals')
    ax1.plot(input, label='Input')
    ax1.plot(prediction, label='Prediction')
    ax1.plot(target, label='Target')
    ax1.set_xlabel('Frequency (Hz)')
    ax1.set_ylabel('Amplitude')
    ax1.grid()
    ax1.legend(loc=1)

    ax2 = fig.add_subplot(312)
    ax2.set_title('Frequency plot of input and filtered signals (FFT size = ' + str(N) + ')')
    ax2.plot(n[:int(N/2)]*fs/N, toDB(abs(X[:int(N/2)])), label='Input')
    ax2.plot(n[:int(N/2)]*fs/N, toDB(abs(Y[:int(N/2)])), label='Prediction')
    ax2.plot(n[:int(N/2)]*fs/N, toDB(abs(Z[:int(N/2)])), label='Target')
    ax2.set_xlabel('Frequency (Hz)')
    ax2.set_ylabel('Magnitude')
    ax2.grid()
    ax2.legend(loc=1)

    Hy = abs(Y/X) # Freq response
    #Hy[Hy>1] = 1 # values greater than 1 are clipped.

    Hz = abs(Z/X)
    #Hz[Hz>1] = 1

    ax3 = fig.add_subplot(313)
    ax3.set_title('Frequency Response of Filter')
    ax3.plot(n[:int(N/2)]*fs/N, toDB(Hy[:int(N/2)]), 'C1', label='Prediction')
    ax3.plot(n[:int(N/2)]*fs/N, toDB(Hz[:int(N/2)]), 'C2', label='Target')
    ax3.axhline(y=-3, color='red', linewidth=0.5)
    ax3.set_xlabel('Frequency (Hz)')
    ax3.set_ylabel('Amplitude (dB)')
    ax3.grid()
    ax3.legend(loc=1)

    if save:
        file_name = 'freq_response'
        if not epoch == None:
            file_name += '_' + str(epoch)
        save_loc = '../../runs/'
        if epoch == 'testrun':
            save_loc = '../../runs/testrun/'
        save_loc = os.path.join(save_loc, save_folder)
        savePlot(fig, name=file_name, loc=save_loc)

    plt.tight_layout()
    plt.show()
    plt.pause(0.0001)
    plt.close()
    #plt.ioff()


def plotSpectrogram(input, prediction, target, save=True, epoch=None, save_folder='', interactive=True):  # TODO: Show colorbar (scale) infront of each subplot.
    f, t, spect1 = stft(input, fs=44100, nperseg=2048)
    f, t, spect2 = stft(target, fs=44100, nperseg=2048)
    f, t, spect3 = stft(prediction, fs=44100, nperseg=2048)

    if interactive:
        plt.ion()

    fig = plt.figure()
    ax1 = fig.add_subplot(411)
    ax1.set_title('Input')
    ax1.pcolormesh(np.arange(len(t)), f, np.abs(spect1), norm=colors.PowerNorm(gamma=0.2))
    #ax1.set_xlabel('Sample no.') # TODO: Set common y-axis label
    ax1.set_ylabel('Frequency (Hz)')

    ax2 = fig.add_subplot(412)
    ax2.set_title('Target')
    ax2.pcolormesh(np.arange(len(t)), f, np.abs(spect2), norm=colors.PowerNorm(gamma=0.2))
    #ax2.set_xlabel('Sample no.')
    ax2.set_ylabel('Frequency (Hz)')

    ax3 = fig.add_subplot(413)
    ax3.set_title('Prediction')
    ax3.pcolormesh(np.arange(len(t)), f, np.abs(spect3), norm=colors.PowerNorm(gamma=0.2))
    #ax3.set_xlabel('Sample no.')
    ax3.set_ylabel('Frequency (Hz)')

    ax4 = fig.add_subplot(414)
    ax4.set_title('Absolute error between target and prediction')
    ax4.pcolormesh(np.arange(len(t)), f, np.abs(np.abs(spect3)-np.abs(spect2)), norm=colors.PowerNorm(gamma=0.2))
    ax4.set_xlabel('Sample no.')
    ax4.set_ylabel('Frequency (Hz)')

    if save:
        file_name = 'spectrogram'
        if not epoch == None:
            file_name += '_' + str(epoch)
        save_loc = '../../runs/'
        if epoch == 'testrun':
            save_loc = '../../runs/testrun/'
        save_loc = os.path.join(save_loc, save_folder)
        savePlot(fig, name=file_name, loc=save_loc)

    plt.tight_layout()
    plt.show()
    plt.close()


def visualizeWeights(model, save=True, epoch=None, save_folder='', interactive=True):
    '''
    Works only for 2 layer (NeuralNet) model.

    :param model: Trained 'NeuralNet' model
    :return: None
    '''
    parameter_names = []
    parameters = []
    for name, param in model.named_parameters():
        if param.requires_grad:
            parameter_names.append(name)
            parameters.append(param.data)
            # print(name, param.data.shape)
            # print(param.data)
            # print('\n\n')

    if interactive:
        plt.ion()
    fig = plt.figure()

    ax1 = fig.add_subplot(211)
    ax1.set_title(parameter_names[0])
    ax1.set_xlabel('Weights')
    ax1.set_ylabel('Input nodes')
    # TODO: Input nodes on Y-axis seem inverted.
    img = ax1.pcolormesh(np.arange(parameters[0].shape[0]), np.arange(parameters[0].shape[1]), parameters[0].cpu().numpy().T, norm=colors.PowerNorm(gamma=1))
    fig.colorbar(img)

    # For hidden_size 1:
    #ax1.bar(np.arange(parameters[0].shape[1]), parameters[0].squeeze(0).cpu().numpy())

    ax1.grid()

    ax2 = fig.add_subplot(212)
    ax2.set_title(parameter_names[2])
    ax2.set_xlabel('Weights')
    ax2.set_ylabel('Weight values')
    ax2.bar(np.arange(parameters[2].shape[1]), parameters[2].squeeze(0).cpu().numpy())
    ax2.grid()

    if save:
        file_name = 'weights'
        if not epoch == None:
            file_name += '_' + str(epoch)
        save_loc = '../../runs/'
        if epoch == 'testrun':
            save_loc = '../../runs/testrun/'
        save_loc = os.path.join(save_loc, save_folder)
        savePlot(fig, name=file_name, loc=save_loc)

    plt.tight_layout()
    plt.show()
    plt.pause(0.0001)
    plt.close()

def toDB(amp):
    return 20 * np.log10(amp)


def savePlot(fig, name, pickle_plot=True, loc='../../runs/'):
    if not os.path.exists(loc):
        os.makedirs(loc)
    fig.savefig(os.path.join(loc, name + '.png'))
    if pickle_plot:
        pickle.dump(fig, gzip.open(os.path.join(loc, name + '.pklz'), 'wb'))