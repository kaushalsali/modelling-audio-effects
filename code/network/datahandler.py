import numpy as np
import torch.utils.data as data
from torch.utils.data.sampler import SubsetRandomSampler, SequentialSampler


def prepare_data_loaders(dataset, split, batch_size, shuffle=False):

    dataset_size = len(dataset)
    train_split = (0, int(split['train'] * dataset_size))
    val_split = (train_split[1], train_split[1] + int(split['val'] * dataset_size))
    test_split = (val_split[1], val_split[1] + int(split['test'] * dataset_size))

    indices = list(range(dataset_size))

    if shuffle:
        # np.random.seed(0)
        np.random.shuffle(indices)

    train_indices = indices[train_split[0]: train_split[1]]
    val_indices = indices[val_split[0]: val_split[1]]
    test_indices = indices[test_split[0]: test_split[1]]

    if shuffle:
        train_sampler = SubsetRandomSampler(train_indices)
        val_sampler = SubsetRandomSampler(val_indices)
        test_sampler = SubsetRandomSampler(test_indices)
    else:
        train_sampler = SequentialSampler(train_indices)
        val_sampler = SequentialSampler(val_indices)
        test_sampler = SequentialSampler(test_indices)

    training_data_loader = data.DataLoader(dataset, batch_size=batch_size, sampler=train_sampler)
    val_data_loader = data.DataLoader(dataset, batch_size=batch_size, sampler=val_sampler)
    test_data_loader = data.DataLoader(dataset, batch_size=batch_size, sampler=test_sampler)

    return training_data_loader, val_data_loader, test_data_loader


def prepare_chirp_data_loaders(dataset_train, dataset_val, dataset_test, batch_size, shuffle=False):

    training_data_loader = data.DataLoader(dataset_train, batch_size=batch_size, shuffle=shuffle)
    val_data_loader = data.DataLoader(dataset_val, batch_size=batch_size, shuffle=shuffle)
    test_data_loader = data.DataLoader(dataset_test, batch_size=batch_size, shuffle=shuffle)

    return training_data_loader, val_data_loader, test_data_loader

'''
# not being used
def prepare_filter_data_loaders(dataset, split, batch_size):
    dataset_size = len(dataset)
    train_split = (0, int(split['train'] * dataset_size))
    val_split = (train_split[1], train_split[1] + int(split['val'] * dataset_size))
    test_split = (val_split[1], val_split[1] + int(split['test'] * dataset_size))

    indices = list(range(dataset_size))

    train_indices = indices[train_split[0]: train_split[1]]
    val_indices = indices[val_split[0]: val_split[1]]
    test_indices = indices[test_split[0]: test_split[1]]

    train_sampler = SubsetRandomSampler(train_indices)
    val_sampler = SubsetRandomSampler(val_indices)
    test_sampler = SubsetRandomSampler(test_indices)

    training_data_loader = data.DataLoader(dataset, batch_size=batch_size, sampler=train_sampler)
    val_data_loader = data.DataLoader(dataset, batch_size=batch_size, sampler=val_sampler)
    test_data_loader = data.DataLoader(dataset, batch_size=batch_size, sampler=test_sampler)

    return training_data_loader, val_data_loader, test_data_loader
'''