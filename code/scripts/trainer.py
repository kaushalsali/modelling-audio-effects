import os
import sys
import shutil

import torch
import torch.nn as nn
import torch.optim as optim
import argparse
import matplotlib
matplotlib.use('TkAgg')

sys.path.append('../network/')
sys.path.append('../network/models/')
import neuralnet
import datasets
from datahandler import prepare_data_loaders, prepare_chirp_data_loaders
import networkutils
import visualizationutils


parser = argparse.ArgumentParser(description='Generate Datasets')
parser.add_argument('--dataset', type=str, default='whitenoise')
parser.add_argument('--train-only', action='store_true', default=False)
parser.add_argument('--test-only', action='store_true', default=False)
parser.add_argument('--model', type=str, default='NeuralNetNoReLU')
parser.add_argument('--input-size', type=int, default=1)
parser.add_argument('--hidden-size', type=int, default=10)
parser.add_argument('--output-size', type=int, default=1)
parser.add_argument('--input-hop', type=int, default=1)
parser.add_argument('--output-hop', type=int, default=1)

parser.add_argument('--epochs', type=int, default=10)
parser.add_argument('--batch-size', type=int, default=10)
parser.add_argument('--learning-rate', type=float, default=0.01)
parser.add_argument('--learning-rate-decay-interval', nargs='+', type=int, default=[9999], help='Number of epochs after which the rate should decay')
parser.add_argument('--learning-rate-decay-factor', type=float, default=0.1, help='Factor to multiply LR')
parser.add_argument('--momentum', type=float, default=0.0)
parser.add_argument('--weight-decay', type=float, default=0.0)
parser.add_argument('--shuffle-data', action='store_true', default=False)

parser.add_argument('--visualize-training', nargs='+', type=str, default=None)
parser.add_argument('--visualize-evaluation', nargs='+', type=str, default=None)
parser.add_argument('--visualize-testing', nargs='+', type=str, default=None)
parser.add_argument('--visualize-interval', type=int, default=10)
parser.add_argument('--log-model', action='store_true', default=False)
parser.add_argument('--no-cuda', action='store_true', default=False)
parser.add_argument('--model-filename', type=str, default='model')

args = parser.parse_args()



whitenoise_config = {
    'filter':'lowpassSecondOrder',
    'low': -1,
    'high': 1,
    'samples': 1*102400,
    'decay': 0.9,
    'fc': None,
    'psi': None,
    'f0': 500,
    'Q': 0.707,
    'S': None,
    'gain': -10,
    'plot': ['signals']
}


# Backup old plots, Delete older plots
if not args.test_only:
    if os.path.exists('../../runs/'):
        if os.path.exists('../../other/runs_old'):
            shutil.rmtree('../../other/runs_old')
        shutil.move('../../runs/', '../../other/runs_old')
        os.makedirs('../../runs/')


# Load, split data and prepare DataLoaders
if args.dataset == 'sine':
    dataset = datasets.SineDataset()

elif args.dataset == 'whitenoise':
    dataset = datasets.WhiteNoiseDataset(input_window_size=args.input_size,
                                         input_hop=args.input_hop,
                                         output_window_size=args.output_size,
                                         output_hop=args.output_hop)
    dataset_split = {'train': 0.7, 'val': 0.15, 'test': 0.15}
    training_data_loader, val_data_loader, test_data_loader = prepare_data_loaders(dataset=dataset,
                                                                                   split=dataset_split,
                                                                                   batch_size=args.batch_size,
                                                                                   shuffle=args.shuffle_data)

elif args.dataset == 'whitenoise-live':
    dataset = datasets.WhiteNoiseLiveDataset(input_window_size=args.input_size,
                                         input_hop=args.input_hop,
                                         output_window_size=args.output_size,
                                         output_hop=args.output_hop,
                                         whitenoise_config=whitenoise_config)
    dataset_split = {'train': 0.7, 'val': 0.15, 'test': 0.15}
    training_data_loader, val_data_loader, test_data_loader = prepare_data_loaders(dataset=dataset,
                                                                                   split=dataset_split,
                                                                                   batch_size=args.batch_size,
                                                                                   shuffle=args.shuffle_data)

elif args.dataset == 'chirp':
    dataset_train = datasets.ChirpDataset(file='train',
                                         input_window_size=args.input_size,
                                         input_hop=args.input_hop,
                                         output_window_size=args.output_size,
                                         output_hop=args.output_hop)

    # Val is 15% of size of train (edit from datasetgenerator)
    dataset_val = datasets.ChirpDataset(file='val',
                                         input_window_size=args.input_size,
                                         input_hop=args.input_hop,
                                         output_window_size=args.output_size,
                                         output_hop=args.output_hop)

    # Val is 30% of size of train  (edit from datasetgenerator)
    dataset_test = datasets.ChirpDataset(file='test',
                                         input_window_size=args.input_size,
                                         input_hop=args.input_hop,
                                         output_window_size=args.output_size,
                                         output_hop=args.output_hop)

    dataset_split = {'train': 0.7, 'val': 0.15, 'test': 0.15}
    training_data_loader, val_data_loader, test_data_loader = prepare_chirp_data_loaders(dataset_train=dataset_train,
                                                                                         dataset_val=dataset_val,
                                                                                         dataset_test=dataset_test,
                                                                                         batch_size=args.batch_size,
                                                                                         shuffle=args.shuffle_data)

# Construct model
if args.model == 'NeuralNet':
    model = neuralnet.NeuralNet(input_size=args.input_size,
                                hidden_layer_size=args.hidden_size,
                                output_layer_size=args.output_size)
elif args.model == 'NeuralNetNoReLU':
    model = neuralnet.NeuralNetNoReLU(input_size=args.input_size,
                                      hidden_layer_size=args.hidden_size,
                                      output_layer_size=args.output_size)
elif args.model == 'NeuralNet2Layer':
    model = neuralnet.NeuralNet2Layer(input_size=args.input_size,
                                      hidden_layer_size=args.hidden_size,
                                      output_layer_size=args.output_size)


# GPU use settings
if not args.no_cuda and torch.cuda.is_available():
    use_cuda = True
    print("\nRunning on GPU")
    model = model.cuda()
else:
    use_cuda = False


# Model Parameters
mse_loss = nn.MSELoss()
mae_loss = nn.L1Loss()
scheduler = optim.lr_scheduler.LambdaLR
optimizer = optim.SGD(model.parameters(), lr=args.learning_rate, momentum=args.momentum, weight_decay=args.weight_decay)
#optimizer = optim.Adam(model.parameters(), lr=args.learning_rate, weight_decay=args.weight_decay)
lr_scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=args.learning_rate_decay_interval, gamma=args.learning_rate_decay_factor)

# Training
if not args.test_only:
    train_loss_cache = []
    val_loss_cache = []
    val_loss = 0
    best_val_loss = 999
    best_train_loss = 999

    print("\n----TRAINING----")

    for epoch in range(args.epochs):
        print("\nEpoch " + str(epoch) + ":" + "\n---------------------------------------")

        lr_scheduler.step()

        train_loss = networkutils.train(model=model,
                                        data_loader=training_data_loader,
                                        loss_criterion=mse_loss,
                                        optimizer=optimizer,
                                        visualize=args.visualize_training,
                                        visualize_interval=args.visualize_interval,
                                        epoch=epoch,
                                        cuda=use_cuda)
        print("Avg training loss:\t" + str(train_loss))
        train_loss_cache.append(train_loss)

        val_loss = networkutils.evaluate(model=model,
                                         data_loader=val_data_loader,
                                         loss_criterion=mse_loss,
                                         visualize=args.visualize_evaluation,
                                         visualize_interval=args.visualize_interval,
                                         epoch=epoch,
                                         cuda=use_cuda)
        print("Avg validation loss:\t" + str(val_loss))
        val_loss_cache.append(val_loss)
        if epoch % 5 == 5:
            visualizationutils.plotLoss(train_loss_cache[-25:], val_loss_cache[-25:], epoch=epoch, num_epochs_to_view=25, save=False, interactive=True)

        if val_loss < best_val_loss:
            best_val_loss = val_loss
            best_train_loss = train_loss
            networkutils.saveModel(model, args.model_filename)
            networkutils.saveModel(model, args.model_filename, path="../../runs/")
    print("Training finished.")

    with open('../../runs/trainer.log','w') as f:
        f.write('Train Loss: ' + str(best_train_loss) + '\n')
        f.write('Val Loss: ' + str(best_val_loss) + '\n')  # Writes losses for best model

    visualizationutils.plotLoss(train_loss_cache, val_loss_cache, save=True, interactive=True)

# Testing
if not args.train_only:
    print("\n----TESTING----")
    model = networkutils.loadModel(args.model_filename)

    if use_cuda and torch.cuda.is_available():
        model.cuda()

    test_loss = networkutils.evaluate(model=model,
                                      data_loader=test_data_loader,
                                      loss_criterion=mse_loss,
                                      visualize=args.visualize_testing,
                                      epoch='test',
                                      cuda=use_cuda)
    print("Test loss:\t" + str(test_loss))
    with open('../../runs/trainer.log','a') as f:
        f.write('Test Loss: ' + str(test_loss) + '\n')


if not args.test_only:  # Run not logged when --test-only
    networkutils.logRun({'model': args.dataset, 'test_loss': test_loss, 'best_val_loss': best_val_loss,
                'input_size': args.input_size, 'hidden_size': args.hidden_size, 'epochs': args.epochs,
                'optimizer': type(optimizer), 'learning_rate': args.learning_rate,
                'learning_rate_decay_interval': args.learning_rate_decay_interval,
                'learning_rate_decay_factor':args.learning_rate_decay_factor, 'momentum': args.momentum,
                'batch_size': args.batch_size, 'weight_decay': args.weight_decay})

if args.log_model:
    networkutils.logModel({'model': args.dataset, 'test_loss': test_loss, 'best_val_loss': best_val_loss,
                'input_size': args.input_size, 'hidden_size': args.hidden_size, 'epochs': args.epochs,
                'optimizer': type(optimizer), 'learning_rate': args.learning_rate,
                'learning_rate_decay_interval': args.learning_rate_decay_interval,
                'learning_rate_decay_factor':args.learning_rate_decay_factor, 'momentum': args.momentum,
                'batch_size': args.batch_size, 'weight_decay': args.weight_decay})
