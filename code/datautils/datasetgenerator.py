import os
import shutil
import argparse
import glob

import wavegenerator as gen
import waveshaper as ws
import filter
import ioutils


parser = argparse.ArgumentParser(description='Generate Datasets')

# General options
parser.add_argument('--dataset', type=str, default='whitenoise', help='Name of dataset to be generated.')
parser.add_argument('--dataset-root', help='Absolute path of the root directory of all datasets.')
parser.add_argument('-i', '--input', default=False, action='store_true', help='Use to generate input data.')
parser.add_argument('-o', '--output', default=False, action='store_true', help='Use to generate output / waveshaped data.')
parser.add_argument('-norm', '--normalize', default=False, action='store_true', help='Use to generate normalized data.')
parser.add_argument('--delete', default=False, action='store_true', help='Delete data')

# Generation options
parser.add_argument('-fs', '--sampling-frequency', type=int, default=44100, help='Sampling frequency in Hz(default: 44100).')
parser.add_argument('-t', '--time', type=float, help='Duration of signal (default: 5s).')
parser.add_argument('-n', '--samples', type=int, help='Samples of signal to be generated.')
parser.add_argument('--amp-range', type=int, default=[0, 1, 1], nargs=3, help='Provide range_start, range_end, step (in dB)')
parser.add_argument('--freq-range', type=int, default=[1, 501, 1], nargs=3, help='Provide range_start, range_end, step')
parser.add_argument('--signals-per-amp', type=int, default=1, help='Number of signals to generate per amplitude')

# Whitenoise options
parser.add_argument('--distribution', type=str, default='uniform', help='Distribution to use for generating whitenoise')
parser.add_argument('--whitenoise-range', type=int, default=[-1,1], nargs=2, help='Low and high limit for uniform distrubution')

# Chirp options
parser.add_argument('--f0', type=int, default=0)
parser.add_argument('--f1', type=int, default=22050)
parser.add_argument('--method', type=str, default='linear')
parser.add_argument('--max-freq-sample', type=int)

# Waveshaping options
parser.add_argument('--waveshaper', default=None, type=str, help='Waveshaping function to be used')

# Filtering options
parser.add_argument('--filter', type=str, default=None)
parser.add_argument('--filter-decay', type=float)
parser.add_argument('--filter-cutoff', type=float)
parser.add_argument('--filter-damping', type=float)
parser.add_argument('--filter-f0', type=float)
parser.add_argument('--filter-gain', type=float)
parser.add_argument('--filter-Q', type=float)
parser.add_argument('--filter-S', type=float)

args = parser.parse_args()

if not (args.input or args.output):
    parser.error('Need to provide atleast one of -i or -o flag. Read more about these flags in the help section.')
if not (args.time or args.samples):
    parser.error('Need to provide atleast one of -t (--time) or -n (--samples) flag. Read more about these flags in the help section.')
if args.output:
    if not (args.waveshaper or args.filter):
        parser.error('Need to provide atleast one of --waveshaper or --filter flag. Read more about these flags in the help section.')

Fs = args.sampling_frequency
t = args.time
dataset = args.dataset

# Calculate number of samples based on time.
if args.time:
    N = Fs * t
else:
    N = args.samples

# Set max_freq_sample for chirp
if args.max_freq_sample:
    max_freq_sample = args.max_freq_sample
else:
    max_freq_sample = N

# Set dataset root
if(args.dataset_root):
    dataset_root = args.dataset_root
else:
    dataset_root = os.path.join(os.path.dirname(os.path.dirname(os.getcwd())), 'data')



def generateSineData(amp_range=range(0,-90,-1), N=N, F=range(1, 501), Fs=Fs, normalize=False):
    in_dir = dataset_root + "/train/sine/input"
    if not os.path.exists(in_dir):
        os.makedirs(in_dir)
    for amp in amp_range:
        for freq in F:
            filename = in_dir + "/sine" + str(abs(amp)).zfill(2) + "_" + str(freq).zfill(5) + ".wav"
            signal = gen.generateSine(amp_in_dB=amp, N=N, F=freq, Fs=Fs, normalize=normalize)
            ioutils.wavwrite(filename, Fs, signal)


def generateChirpData(amp_range=range(0,-90,-1), F0=0, F1=Fs/2, N=N, Fs=Fs, max_freq_sample=N, method='linear', normalize=False):
    '''
    Generates seperate train, val and test files.
    val is 15% of size of train signal
    test is 30% of size of train signal
    '''
    in_dir = dataset_root + "/chirp/train/input"
    if not os.path.exists(in_dir):
        os.makedirs(in_dir)
    if os.path.exists(os.path.join(in_dir, '.DS_Store')):
        os.remove(os.path.join(in_dir, '.DS_Store'))
    t = N/Fs
    t1 = max_freq_sample/Fs
    for amp in amp_range:
        filename = in_dir + "/chirp" + str(abs(amp)).zfill(2) + "_train.wav"
        signal = gen.generateChirp(amp_in_dB=amp, F0=F0, F1=F1, duration=t, max_freq_time=t1, Fs=Fs, method=method, normalize=normalize)
        ioutils.wavwrite(filename, Fs, signal)
        filename = in_dir + "/chirp" + str(abs(amp)).zfill(2) + "_val.wav"
        signal = gen.generateChirp(amp_in_dB=amp, F0=F0, F1=F1, duration=t*0.15, max_freq_time=t1*0.15/3, Fs=Fs, method=method, normalize=normalize)
        ioutils.wavwrite(filename, Fs, signal)
        filename = in_dir + "/chirp" + str(abs(amp)).zfill(2) + "_test.wav"
        signal = gen.generateChirp(amp_in_dB=amp, F0=F0, F1=F1, duration=t*0.3, max_freq_time=t1*0.075, Fs=Fs, method=method, normalize=normalize)
        ioutils.wavwrite(filename, Fs, signal)

    
def generateWhiteNoiseData(amp_range=range(0,-90,-1), low=-1, high=1, mean=0, std=1, N=N, distribution='uniform', normalize=False, signals_per_amp=1):
    in_dir = dataset_root + "/whitenoise/train/input"
    if not os.path.exists(in_dir):
        os.makedirs(in_dir)
    if os.path.exists(os.path.join(in_dir, '.DS_Store')):
        os.remove(os.path.join(in_dir, '.DS_Store'))

    for amp in amp_range:
        for sample in range(signals_per_amp):
            filename = in_dir + "/whitenoise" + str(abs(amp)).zfill(2) + "_" + str(sample).zfill(2) + ".wav"
            signal = gen.generateWhiteNoise(amp_in_dB=amp,low=low, high=high, mean=mean, std=std, N=N, distribution=distribution, normalize=normalize)
            #print(max(signal),min(signal))
            ioutils.wavwrite(filename, Fs, signal)


def generateWaveshapedData(waveshaper, dataset, normalize=False):    
    global dataset_root
    in_dir = dataset_root + "/" + dataset + "/train/input"
    out_dir = dataset_root + "/" + dataset + "/train/target"
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    if os.path.exists(os.path.join(out_dir, '.DS_Store')):
        os.remove(os.path.join(out_dir, '.DS_Store'))
    files = sorted(os.listdir(in_dir))
    for each_file in files:
        fs, signal = ioutils.wavread(os.path.join(in_dir, each_file))
        ioutils.wavwrite(filename=out_dir + '/' + each_file[:-4] + '_' + args.waveshaper + '.wav',
                 fs=fs,
                 signal=ws.shapeWave(signal, waveshaper, normalize))
    

def generateFilteredData(dataset, filter_type):
    in_dir = dataset_root + "/" + dataset + "/train/input"
    out_dir = dataset_root + "/" + dataset + "/train/target"
    output_signal = None
    output_filename = None
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    if os.path.exists(os.path.join(out_dir, '.DS_Store')):
        os.remove(os.path.join(out_dir, '.DS_Store'))
    files = sorted(glob.iglob(os.path.join(in_dir, '*.wav')))
    for each_file in files:
        fs, signal = ioutils.wavread(os.path.join(in_dir, each_file))
        if filter_type == 'lowpassFirstOrder':
            output_signal = filter.lowpassFirstOrder(signal, decay=args.filter_decay)
            output_filename = os.path.join(out_dir, os.path.basename(each_file)[:-4] + '_' + args.filter + '_' + str(args.filter_decay) + '.wav')
        elif filter_type == 'lowpassSecondOrder':
            #output_signal = filter.lowpassSecondOrder(signal, fc=args.filter_cutoff, psi=args.filter_damping)
            #output_filename = os.path.join(out_dir, os.path.basename(each_file)[:-4] + '_' + args.filter + '_' + str(int(args.filter_cutoff)) + '_' + str(args.filter_damping) + '.wav')
            output_signal = filter.lowpassSecondOrder(signal, f0=args.filter_f0, Q=args.filter_Q, fs=Fs)
            output_filename = os.path.join(out_dir, os.path.basename(each_file)[:-4] + '_' + args.filter + '_' + str(int(args.filter_f0)) + '_Q' + str(args.filter_Q) + '.wav')
        elif filter_type == 'highpassSecondOrder':
            output_signal = filter.highpassSecondOrder(signal, f0=args.filter_f0, Q=args.filter_Q, fs=Fs)
            output_filename = os.path.join(out_dir, os.path.basename(each_file)[:-4] + '_' + args.filter + '_' + str(int(args.filter_f0)) + '_Q' + str(args.filter_Q) + '.wav')
        elif filter_type == 'lowshelfSecondOrder':
            output_signal = filter.lowshelfSecondOrder(signal, f0=args.filter_f0, S=args.filter_S, gain=args.filter_gain, fs=Fs)
            output_filename = os.path.join(out_dir, os.path.basename(each_file)[:-4] + '_' + args.filter + '_' + str(int(args.filter_f0)) + '_S' + str(args.filter_S) + '_' + str(args.filter_gain) + '.wav')
        elif filter_type == 'highshelfSecondOrder':
            output_signal = filter.highshelfSecondOrder(signal, f0=args.filter_f0, S=args.filter_S, gain=args.filter_gain, fs=Fs)
            output_filename = os.path.join(out_dir, os.path.basename(each_file)[:-4] + '_' + args.filter + '_' + str(int(args.filter_f0)) + '_S' + str(args.filter_S) + '_' + str(args.filter_gain) + '.wav')
        elif filter_type == 'peakSecondOrder':
            output_signal = filter.peakSecondOrder(signal, f0=args.filter_f0, Q=args.filter_Q, gain=args.filter_gain, fs=Fs)
            output_filename = os.path.join(out_dir, os.path.basename(each_file)[:-4] + '_' + args.filter + '_' + str(int(args.filter_f0)) + '_Q' + str(args.filter_Q) + '_' + str(args.filter_gain) + '.wav')
        ioutils.wavwrite(filename=output_filename, fs=fs, signal=output_signal)


def deleteDataset(dataset, input, output):
    global dataset_root
    dataset_dir = os.path.join(dataset_root, dataset, 'train')
    if input:
        directory = os.path.join(dataset_dir, 'input')
        if os.path.exists(directory):
            shutil.rmtree(directory)
    if output:
        directory = os.path.join(dataset_dir, 'target')
        if os.path.exists(directory):
            shutil.rmtree(directory)


if args.delete:
    deleteDataset(dataset, args.input, args.output)

else:

    if args.input:
        deleteDataset(dataset, input=True, output=False)

        if dataset == 'chirp':
            generateChirpData(amp_range=range(*args.amp_range),
                              F0=args.f0,
                              F1=args.f1,
                              max_freq_sample=max_freq_sample,
                              method=args.method,
                              normalize=args.normalize)
        elif dataset == 'whitenoise':
            generateWhiteNoiseData(amp_range=range(*args.amp_range),
                                   signals_per_amp=args.signals_per_amp,
                                   distribution=args.distribution,
                                   low=args.whitenoise_range[0],
                                   high=args.whitenoise_range[1],
                                   normalize=args.normalize,)
        elif dataset == 'sine':
            generateSineData(normalize=args.normalize,
                             amp_range=range(*args.amp_range),
                             F=range(*args.freq_range))

    if args.output:
        deleteDataset(dataset, input=False, output=True)

        if args.waveshaper:
            generateWaveshapedData(waveshaper=ws.waveshape_functions[args.waveshaper],
                                   dataset=dataset,
                                   normalize=args.normalize)
        elif args.filter:
            generateFilteredData(dataset=dataset,
                                 filter_type=args.filter)
