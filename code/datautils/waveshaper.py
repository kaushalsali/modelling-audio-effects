import numpy as np

waveshape_functions = {
    'poly': lambda x: 2*x*x*x + 3*x,
    'square': lambda x: x*x,
    'sigmoid': lambda x: 1/(1+np.exp(-x)),
    'softclip': lambda x: softclip(x),
    'distortion': lambda x: distortion(x),
    'a': lambda x: np.sin(x)*np.cos(x) - x/abs(x)*0.5
}


def softclip(x):
    y = None
    if abs(x) >= 0 and abs(x) < 1/3:
        y = 2*x
    elif abs(x) >= 1/3 and abs(x) < 2/3:
        y = ((3-pow((2-3*abs(x)), 2))/3) * (x/abs(x))
    elif abs(x) > 2/3:
        y = x/abs(x)
    return y

'''
def distortion(x, gain=1, mix=1):
    q = x * gain / max(abs(x))
    z = q/max(abs(q)) * (1-np.exp(pow(q, 2)/abs(q)))
    y = mix * z * max(abs(x)) / max(abs(z)) + (1-mix) * x
    return y
'''

def distortion(x):
    #print('a')
    return np.sign(-x) * (1-np.exp(np.sign(-x) * x))


def normalizeSignal1(signal): # Normalize signal -1 to 1
    return 2 * ((signal - np.min(signal)) / (np.max(signal) - np.min(signal))) - 1


def normalizeSignal(signal):  # ie gain=1
    return signal / max(abs(signal))

def shapeWave(signal, waveshape_function, normalize=False):
    waveshaped_signal = np.array(list(map(waveshape_function, signal)))
    if normalize:
        waveshaped_signal = normalizeSignal(waveshaped_signal)
    return waveshaped_signal

