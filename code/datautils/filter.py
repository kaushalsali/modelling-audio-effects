import numpy as np
import scipy.signal as sp

'''
Available Filters:
    
    FIR Filters:
        Second Order:
            - Lowpass
            - Highpass

    IIR Filters:
        First Order:
            - Lowpass
        
        Second Order:
            - Lowpass
            - Highpass
            - Peak
            - Lowshelf
            - Highshelf

'''


def lowpassFirstOrder(signal, decay):
    # y = b*x[n] + (1-b)*y[n-1]
    b = 1 - decay
    y = 0
    out = []
    for x in signal:
        y += b * (x - y)
        out.append(y)
    return np.array(out)


# def highpass(signal):
#     # y(n) = d(n) - d(n-1)
#     prev = 0
#     out = []
#     for x in signal:
#         out.append(x - prev)
#         prev = x
#     return np.array(out)


#
#---- Old implementation---
#
# def lowpassSecondOrder(signal, fc, psi, fs=44100):
#     '''
#     Second order lowpass filter:
#         y(n) = b0 * x(n) + b1 * x(n-1) + b2 * x(n - 2) - a1 * y(n - 1) - a2 * y(n - 2)
#     '''
#
#     C = 1/(np.tan(np.pi * fc / fs))
#     b0 = 1/(1+ 2*psi*C + C**2)
#     b1 = 2 * b0
#     b2 = b0
#     a1 = 2 * b0 * (1 - C**2)
#     a2 = b0 * (1 - 2*psi*C + C**2)
#
#     signal = np.insert(np.insert(signal, 0, 0), 0, 0)
#
#     out = np.array([0,0])
#     y = 0
#
#     for i in range(2, len(signal)):
#         y = b0*signal[i] + b1*signal[i-1] + b2*signal[i-2] - a1*out[i-1] - a2*out[i-2]
#         out = np.append(out, y)
#     return out[2:]


def lowpassSecondOrder(signal, f0, Q, fs=44100):
    b, a = getLowpassSecondOrderCoefficients(f0=f0, Q=Q, fs=fs)
    return filterSecondOrder(signal, b, a)

def getLowpassSecondOrderCoefficients(f0, Q, fs):
    omega0 = 2 * np.pi * f0/fs
    alpha = np.sin(omega0)/(2*Q)
    b = np.zeros(3)
    a = np.zeros(3)
    b[0] = (1 - np.cos(omega0))/2
    b[1] = (1 - np.cos(omega0))
    b[2] = b[0]
    a[0] = 1 + alpha
    a[1] = -2*np.cos(omega0)
    a[2] = 1 - alpha
    return b, a

def highpassSecondOrder(signal, f0, Q, fs=44100):
    b, a = getHighpassSecondOrderCoefficients(f0=f0, Q=Q, fs=fs)
    return filterSecondOrder(signal, b, a)

def getHighpassSecondOrderCoefficients(f0, Q, fs=44100):
    omega0 = 2 * np.pi * f0/fs
    alpha = np.sin(omega0)/(2*Q)
    b = np.zeros(3)
    a = np.zeros(3)
    b[0] = (1 + np.cos(omega0))/2
    b[1] = -(1 + np.cos(omega0))
    b[2] = b[0]
    a[0] = 1 + alpha
    a[1] = -2 * np.cos(omega0)
    a[2] = 1 - alpha
    return b, a



def peakSecondOrder(signal, f0, Q, gain, fs=44100):
    b, a = getPeakSecondOrderCoefficients(f0=f0, Q=Q, gain=gain, fs=fs)
    return filterSecondOrder(signal, b, a)

def getPeakSecondOrderCoefficients(f0, Q, gain, fs=44100):
    A = 10 ** (gain / 40);
    omega0 = 2 * np.pi * f0/fs
    alpha = np.sin(omega0)/(2*Q)
    b = np.zeros(3)
    a = np.zeros(3)
    b[0] = 1 + alpha * A
    b[1] = -2 * np.cos(omega0)
    b[2] = 1 - alpha * A
    a[0] = 1 + alpha / A
    a[1] = -2 * np.cos(omega0)
    a[2] = 1 - alpha / A
    return b, a



def lowshelfSecondOrder(signal, f0, S, gain, fs=44100):
    b, a = getLowshelfSecondOrderCoefficients(f0=f0, S=S, gain=gain, fs=fs)
    return filterSecondOrder(signal, b, a)

def getLowshelfSecondOrderCoefficients(f0, S, gain, fs=44100):
    A = 10 ** (gain / 40);
    omega0 = 2 * np.pi * f0/fs
    alpha = np.sin(omega0)/2 * np.sqrt((A + 1/A) * (1/S - 1) + 2)
    b = np.zeros(3)
    a = np.zeros(3)
    b[0] = A * ((A+1) - (A-1)*np.cos(omega0) + 2*np.sqrt(A)*alpha)
    b[1] = 2 * A * ((A-1) - (A+1)*np.cos(omega0))
    b[2] = A * ((A+1) - (A-1)*np.cos(omega0) - 2*np.sqrt(A)*alpha)
    a[0] = (A+1) + (A-1)*np.cos(omega0) + 2*np.sqrt(A)*alpha
    a[1] = -2 * ((A-1) + (A+1)*np.cos(omega0))
    a[2] = (A+1) + (A-1)*np.cos(omega0) - 2*np.sqrt(A)*alpha
    return b, a



def highshelfSecondOrder(signal, f0, S, gain, fs=44100):
    b, a = getHighshelfSecondOrderCoefficients(f0=f0, S=S, gain=gain, fs=fs)
    return filterSecondOrder(signal, b, a)

def getHighshelfSecondOrderCoefficients(f0, S, gain, fs=44100):
    A = 10 ** (gain / 40);
    omega0 = 2 * np.pi * f0/fs
    alpha = np.sin(omega0)/2 * np.sqrt((A + 1/A) * (1/S - 1) + 2)
    b = np.zeros(3)
    a = np.zeros(3)
    b[0] = A * ((A+1) + (A-1)*np.cos(omega0) + 2*np.sqrt(A)*alpha)
    b[1] = -2 * A * ((A-1) + (A+1)*np.cos(omega0))
    b[2] = A * ((A+1) + (A-1)*np.cos(omega0) - 2*np.sqrt(A)*alpha)
    a[0] = (A+1) - (A-1)*np.cos(omega0) + 2*np.sqrt(A)*alpha
    a[1] = 2 * ((A-1) - (A+1)*np.cos(omega0))
    a[2] = (A+1) - (A-1)*np.cos(omega0) - 2*np.sqrt(A)*alpha
    return b, a



def filterSecondOrder(signal, b, a):
    out = np.zeros(len(signal))
    y1 = 0
    y2 = 0
    x1 = 0
    x2 = 0
    for i in range(len(signal)):
        out[i] = (b[0]*signal[i] + b[1]*x1 + b[2]*x2 - a[1]*y1 - a[2]*y2)/a[0]
        y2 = y1
        y1 = out[i]
        x2 = x1
        x1 = signal[i]
    return out


def lowpassFIR(signal, order, f0):
    coefs = sp.firwin(numtaps=order+1, cutoff=f0, fs=44100) # numtaps = order + 1
    return sp.lfilter(coefs, [1.0], signal)


def highpassSecondOrderFIR(signal):
    pass

def toDB(amp):
    return 20 * np.log10(amp)
