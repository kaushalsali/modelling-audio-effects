import numpy as np
import sys
import os
import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt
from scipy.io.wavfile import read, write
import copy
import argparse
import shutil
sys.path.append('../datautils')
import waveshaper as ws
import filter
import ioutils

parser = argparse.ArgumentParser(description='Generate Datasets')

# General options
parser.add_argument('--file-indices', type=int, nargs='+', default=None)
parser.add_argument('--save-format', type=str, default=None)
parser.add_argument('--scaling-factor', type=int, default=None)

# Waveshaping options
parser.add_argument('--waveshaper', type=str, default=None)

# Filtering options
parser.add_argument('--filter', type=str, default=None)
parser.add_argument('--filter-decay', type=float)
parser.add_argument('--filter-cutoff', type=float)
parser.add_argument('--filter-damping', type=float)
parser.add_argument('--filter-f0', type=float)
parser.add_argument('--filter-gain', type=float)
parser.add_argument('--filter-Q', type=float)
parser.add_argument('--filter-S', type=float)

# Visualization options
parser.add_argument('--plot-waveshaper', action='store_true', default=False)
parser.add_argument('--plot-signals', action='store_true', default=False)
args = parser.parse_args()

def plotWave(signals_list):
    plt.ion()
    signals_list = np.array(signals_list)
    if np.max(signals_list[1][0]) > np.max(signals_list[0][0]):
        signals_list = np.flip(signals_list, axis=0)
    for each in signals_list:
        plt.plot(each[0], label=each[1])
        #plt.ylim(-1.1, 1.1)
    plt.grid()
    plt.legend()
    plt.show()
    plt.pause(1)
    plt.close()

def showTestFileIndices():
    loc = '../../data/test/input/'
    if os.path.exists(os.path.join(loc, '.DS_Store')):
        os.remove(os.path.join(loc, '.DS_Store'))
    files = sorted(os.listdir(loc))
    print('\nAvailable Test Files:')
    for i,each in enumerate(files):
        print(i,each)

def getTestFiles():
    loc = '../../data/test/input/'
    if os.path.exists(os.path.join(loc, '.DS_Store')):
        os.remove(os.path.join(loc, '.DS_Store'))
    return sorted(os.listdir(loc))

if args.file_indices is None:
    args.file_indices = range(len(getTestFiles()))
input_path = '../../data/test/input/'
output_path = '../../data/test/target/'

# Remove .DS_Store
if os.path.exists(os.path.join(input_path, '.DS_Store')):
    os.remove(os.path.join(input_path, '.DS_Store'))
shutil.rmtree(output_path)
os.makedirs(output_path)


showTestFileIndices()
print("\nFiles being shaped:", args.file_indices, "\n")

# Read input files
files = sorted(os.listdir(input_path))
for i in args.file_indices:
    print("Processing file:", i)
    fs, signal = ioutils.wavread(os.path.join(input_path, files[i]))

    # Scale input signal
    if args.scaling_factor is not None:
        signal *= args.scaling_factor
        ioutils.wavwrite(os.path.join(input_path, files[i]), fs, signal) # Rewrite input signal

    # Shape output
    shaped_signal = None
    output_filename = None
    if args.waveshaper:
        shaped_signal = ws.shapeWave(signal=signal, waveshape_function=ws.waveshape_functions[args.waveshaper])
        output_filename = os.path.join(output_path, files[i][:-4] + '_' + args.waveshaper + '.wav')
    elif args.filter:
        if args.filter == 'lowpassFirstOrder':
            shaped_signal = filter.lowpassFirstOrder(signal, decay=args.filter_decay)
            output_filename = os.path.join(output_path, os.path.basename(files[i])[:-4] + '_' + args.filter + '_' + str(args.filter_decay) + '.wav')
        elif args.filter == 'lowpassSecondOrder':
            #shaped_signal = filter.lowpassSecondOrder(signal, fc=args.filter_cutoff, psi=args.filter_damping)
            #output_filename = os.path.join(output_path, os.path.basename(files[i])[:-4] + '_' + args.filter + '_' + str(int(args.filter_cutoff)) + '_' + str(args.filter_damping) + '.wav')
            shaped_signal = filter.lowpassSecondOrder(signal, f0=args.filter_f0, Q=args.filter_Q, fs=fs)
            output_filename = os.path.join(output_path, os.path.basename(files[i])[:-4] + '_' + args.filter + '_' + str(int(args.filter_f0)) + '_Q' + str(args.filter_Q) + '.wav')
        elif args.filter == 'highpassSecondOrder':
            shaped_signal = filter.highpassSecondOrder(signal, f0=args.filter_f0, Q=args.filter_Q, fs=fs)
            output_filename = os.path.join(output_path, os.path.basename(files[i])[:-4] + '_' + args.filter + '_' + str(int(args.filter_f0)) + '_Q' + str(args.filter_Q) + '.wav')
        elif args.filter == 'lowshelfSecondOrder':
            shaped_signal = filter.lowshelfSecondOrder(signal, f0=args.filter_f0, S=args.filter_S, gain=args.filter_gain, fs=fs)
            output_filename = os.path.join(output_path, os.path.basename(files[i])[:-4] + '_' + args.filter + '_' + str(int(args.filter_f0)) + '_S' + str(args.filter_S) + '_' + str(args.filter_gain) + '.wav')
        elif args.filter == 'highshelfSecondOrder':
            shaped_signal = filter.highshelfSecondOrder(signal, f0=args.filter_f0, S=args.filter_S, gain=args.filter_gain, fs=fs)
            output_filename = os.path.join(output_path, os.path.basename(files[i])[:-4] + '_' + args.filter + '_' + str(int(args.filter_f0)) + '_S' + str(args.filter_S) + '_' + str(args.filter_gain) + '.wav')
        elif args.filter == 'peakSecondOrder':
            shaped_signal = filter.peakSecondOrder(signal, f0=args.filter_f0, Q=args.filter_Q, gain=args.filter_gain, fs=fs)
            output_filename = os.path.join(output_path, os.path.basename(files[i])[:-4] + '_' + args.filter + '_' + str(int(args.filter_f0)) + '_Q' + str(args.filter_Q) + '_' + str(args.filter_gain) + '.wav')

    # Save shaped signal
    if 'wav' in args.save_format:
        ioutils.wavwrite(output_filename, fs, shaped_signal)
    if 'npy' in args.save_format:
        np.save(output_filename[:-4], shaped_signal)

    # Plot signals
    if args.plot_signals:
        plotWave([[signal,'input'], [shaped_signal,'target']])
    if args.plot_waveshaper:
        plt.plot([[signal,'input'], [shaped_signal,'target']], 'o')
        plt.ylim(-1.1, 1.1)
        plt.show()

print("Done")