import numpy as np
from waveshaper import normalizeSignal
from scipy.signal import chirp

def dBToAmp(dB):    # dB = 20 * np.log10(amplitude)
    amplitude = 10**(dB/20)
    return amplitude

def generateSine(N, F, amp_in_dB=0, Fs=44100, normalize=False):
    A = dBToAmp(amp_in_dB)
    n = np.arange(N)
    signal = A * np.cos(2 * np.pi * F/Fs * n)
    if normalize:
        return A * normalizeSignal(signal)
    else:    
        return A * signal

def generateChirp(F0, F1, duration, max_freq_time, amp_in_dB=0, Fs=44100, method='linear', normalize=False):
    A = dBToAmp(amp_in_dB)
    signal = chirp(np.linspace(0, duration, Fs*duration), f0=F0, f1=F1, t1=max_freq_time, method=method)
    if normalize:
        return A * normalizeSignal(signal)
    else:
        return A * signal
    

def generateWhiteNoise(amp_in_dB=0, low=-1, high=1, mean=0, std=1, N=1000, distribution='uniform', normalize=False):
    A = dBToAmp(amp_in_dB)
    if distribution == 'uniform':
        signal = np.random.uniform(low, high, size=N)
    elif distribution == 'standard_normal':
        signal = np.random.normal(mean, std, size=N)
    if normalize:
        return A * normalizeSignal(signal)
    else:
        return A * signal

