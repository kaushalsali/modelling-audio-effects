import os
import copy
import numpy as np
from scipy.io.wavfile import write, read

INT16_FAC = (2**15)-1
INT32_FAC = (2**31)-1
INT64_FAC = (2**63)-1
norm_fact = {'int16': INT16_FAC, 'int32': INT32_FAC, 'int64': INT64_FAC, 'float32': 1.0, 'float64': 1.0}

def wavread(filename):
    if (os.path.isfile(filename) == False):
        raise ValueError("Input file is wrong")
    fs, x = read(filename)
    if (len(x.shape) !=1):
        raise ValueError("Audio file should be mono")
    if (fs !=44100):
        raise ValueError("Sampling rate of input sound should be 44100")
    x = np.float32(x)/norm_fact[x.dtype.name]
    return fs, x

def wavwrite(filename, fs, signal):
    x = copy.deepcopy(signal)
    x = np.clip(x, -1, 1)
    x *= INT16_FAC
    x = np.int16(x)
    write(filename, fs, x)
