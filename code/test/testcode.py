import sys
import math
import numpy as np
from scipy.fftpack import fft
from scipy.io.wavfile import write,read
from matplotlib import pyplot as plt
sys.path.append('../datautils')
from wavegenerator import *
from waveshaper import *
sys.path.append('/Users/kaushal/Kaushal/Repositories/sms-tools/software/models/')
from utilFunctions import wavread, wavwrite


def plotWave(signals_list):
    n = np.arange(len(signals_list[0]))
    for each in signals_list:
        plt.plot(n,each)
    plt.xlabel('n')
    plt.ylabel('amplitude')
    plt.show()

def plotShapedWave(original, shaped):
    n = np.arange(len(original))
    plt.subplot(2,1,1)
    plt.plot(original, shaped)
    plt.grid(True)
    plt.subplot(2,1,2)
    plt.plot(n,original)
    plt.plot(n,shaped)
    plt.grid(True)
    plt.show()


#---------------------------------------------------------------
'''
N = 44100 * 5
n = np.arange(N)

fs,x = read('../../data/whitenoise/input/whitenoise00.wav')
plotWave([x])
#print(x)
#nx = normalizeSignal(x)

#y = generateWhiteNoise(amp_in_dB=-1, N=44100*5, normalize=True)
#y1 = normalizeSignal(y)
#print(y)
#print(max(abs(y)))

#write('y.wav',44100,y)
#write('y1.wav',44100,y1)
#plotWave([y])

'''
#---------------------------------------------------------------

'''

# Sine Sweep DFT

Fs=44100
t=10
N = Fs*t
n = np.arange(N)
n_Hz = n * Fs / N

sweep = generateChirp(amp_in_dB=0, F0=0,F1=22050, duration=t)
wavwrite(sweep, Fs, "sweep.wav")

X = fft(sweep)

plt.subplot(2,1,1)
plt.plot(n_Hz,sweep)
plt.subplot(2,1,2)
plt.plot(n_Hz[:int(len(n_Hz)/2+1)], X[:int(len(n_Hz)/2+1)])

plt.show()

'''

#---------------------------------------------------------------


#----------------------------------------------------------------

'''

# DFT of White Noise

Fs = 44100
t = 2
N = Fs*t
n = np.arange(N)
n_Hz = n * Fs / N

noise = generateWhiteNoise(mean=0, std=1, N=N)
plt.plot(n,noise)
plt.show()
wavwrite(noise, Fs, "noise.wav")


X = fft(noise)
mX = abs(X)#20*np.log10(abs(X))

plt.subplot(2,1,1)
plt.plot(n_Hz,noise)
plt.subplot(2,1,2)
plt.plot(n_Hz,mX)

plt.show()

'''

#----------------------------------------------------------------

'''

#DFT of Sine

N= 1000
Fs = 44100

n = np.arange(N)


x = generateSine(-20, 1000, 440)
plt.plot(n, x)
plt.show()

M = N
m = np.arange(M)
m_Hz = m * Fs / M

hM1 = int(math.floor((M+1)/2))
hM2 = int(math.floor(M/2))

X = fft(x)
mX = 20*np.log10(abs(X))

plt.subplot(2,1,1)
plt.plot(n,x)
plt.subplot(2,1,2)
plt.plot(m_Hz[:int(len(m_Hz)/2 +1)],mX[:int(len(m_Hz)/2 +1)])

plt.show()

'''

#----------------------------------------------------------------

'''

# Waveshaping and writing as WAV

Fs = 44100
time = 2

x1 = generateSine(0, Fs*time, 440)
wavwrite(x1,Fs,"sine0.wav")
#plotWave(x1)

x1n = x1/ np.max(np.abs(x1))
#wavwrite(x1n,Fs,"sine0norm.wav")
#plotWave(x1n)

waveshape_functions = {
    'poly': lambda x: 2*x*x*x + 3*x,
    'square': lambda x: x*x,
    'sigmoid': lambda x: np.exp(x)/(1+np.exp(x))
}

sx1 = shapeWave(x1, waveshape_functions['poly'], normalize=False)
wavwrite(sx1,Fs,"sine0shaped.wav")
plotShapedWave(x1, sx1)

sx1n = normalizeSignal(sx1)
wavwrite(sx1n,Fs,"sine0shapednorm.wav")
plotWave([x1,sx1,sx1n])

read_fs,read_x = wavread('sine0shaped.wav')
plotWave([sx1[:100],read_x[:100]])

#x2 = generateSine(-10, Fs*time, 440)
#x3 = generateSine(-90, Fs*time, 440)

#wavwrite(x2,Fs,"sine10.wav")
#wavwrite(x3,Fs,"sine90.wav")
#plotWave(x2)
#plotWave(x3)

'''

#----------------------------------------------------------------

'''

# General Demo of Code

y1 = generateSine(1000,100)
y2 = generateSine(1000,800)
x = y1  + y2

waveshape_function = lambda x: x#2*x*x*x + 3*x
x1 = shapeWave(x, waveshape_function)

n = np.arange(len(x))

plt.subplot(2,1,1)
plt.plot(x,x1)
plt.grid(True)

plt.subplot(2,1,2)
plt.plot(n,x)
plt.plot(n,x1)
plt.grid(True)
plt.show()

'''

#---------------------------------------------------------------

'''

# For testing waveshaper

x = generateSine(20,10000)

waveshape_function = lambda x: 2*x*x*x + 3*x
x1 = shapeWave(x, waveshape_function)

n = np.arange(len(x))
print(x)
print(x1)

plt.subplot(2,1,1)
plt.plot(x,x1)
plt.grid(True)

plt.subplot(2,1,2)
plt.plot(n,x)
plt.plot(n,x1)
plt.grid(True)
plt.show()

'''

#---------------------------------------------------------------
