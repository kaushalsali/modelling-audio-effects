import numpy as np
import sys
import os
from matplotlib import pyplot as plt
from scipy.io.wavfile import read
sys.path.append('../datautils')
from scipy.stats import norm
from waveshaper import shapeWave


def plotWave(signals_list, marker='o'):
    n = np.arange(len(signals_list[0]))
    for each in signals_list:
        #plt.plot(each, norm.pdf(each), marker=marker)
        plt.plot(n, each, marker)
    plt.xlabel('n')
    plt.ylabel('amplitude')
    plt.ylim(-5, 5)
    plt.grid()
    plt.show()


dataset_root = os.path.join(os.path.dirname(os.path.dirname(os.getcwd())), 'data')


def plotAllData(dataset):
    whitenoise_root = os.path.join(dataset_root, dataset)
    input_files = sorted(os.listdir(os.path.join(whitenoise_root, 'input')))
    target_files = sorted(os.listdir(os.path.join(whitenoise_root, 'target')))
    for i in range(len(input_files)):
        fs1, x1 = read(os.path.join(whitenoise_root, 'input', input_files[i]))
        fs2, x2 = read(os.path.join(whitenoise_root, 'target', target_files[i]))
        plotWave([x1], '.')


def testAllData():
    sine_root = os.path.join(dataset_root, 'whitenoise/target')
    input_files = os.listdir(sine_root)
    max = 0
    cache = []
    for each in input_files:
        #print(each)
        fs,signal = read(os.path.join(sine_root, each))
        cache.append(signal)
        m = np.max(abs(signal))
        if m>max:
            max=m
        print(signal)
        #print(fs)
        #plotWave([signal])
    print(max)
    plt.plot(np.arange(len(cache)), cache)
    plt.show()

#----------------------------------------------------------------------

plotAllData('whitenoise')
#testAllData()







