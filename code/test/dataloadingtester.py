import numpy as np
import torch
import sys
import torchaudio
from matplotlib import pyplot as plt
from scipy.io.wavfile import read
sys.path.append('../datautils')
from waveshaper import shapeWave
sys.path.append('../network')
from datasets import WhiteNoiseDataset, SineDataset
from datahandler import prepare_data_loaders
from torch.autograd import Variable


def plotWave(signals_list):
    n = np.arange(len(signals_list[0]))
    for each in signals_list:
        plt.plot(n,each)
    plt.xlabel('n')
    plt.ylabel('amplitude')
    plt.show()

def plotShapedWave(original, shaped):
    n = np.arange(len(original))
    plt.subplot(2, 1, 1)
    plt.plot(original, shaped)
    plt.grid(True)
    plt.subplot(2, 1, 2)
    plt.plot(n, original)
    plt.plot(n, shaped)
    plt.grid(True)
    plt.show()


waveshape_functions = {
    'poly': lambda x: 2*x*x*x + 3*x,
    'square': lambda x: x*x,
    'sigmoid': lambda x: np.exp(x)/(1+np.exp(x))
}


def testWhiteNoise():
    fs1, x = read('../../data/whitenoise/input/whitenoise00.wav')
    y, fs2 = torchaudio.load('../../data/whitenoise/input/whitenoise00.wav', normalization=True)

    data = WhiteNoiseDataset()
    in_sample, output_sample = data.__getitem__(0)
    shaped = shapeWave(in_sample, waveshape_functions['sigmoid'])

    print(x[:20])
    print(y[:20])
    #print(in_sample[:20])

    plotWave([x])
    plotWave([y.numpy()])
    plotWave([x, y.numpy(), in_sample.numpy()])
    plotWave([in_sample.numpy(), output_sample.numpy(), shaped])


def testSine():
    train_set = SineDataset()
    train_loader = torch.utils.data.DataLoader(train_set, batch_size=1, shuffle=False)
    for i,o in train_loader:
        print(i)
        print(o)
        print("------------")
        plotWave([i[0].numpy(), o[0].numpy()])



def testLoader(data_loader, batch_size):
    n = np.arange(8)
    for batch in data_loader:
        print(batch_size)
        #for i in range(batch_size):
            #print(len(batch[0][i].numpy()))
        plt.plot(n, batch[0][0].numpy())
        plt.plot(n, batch[1][0].numpy())
        plt.show()


def testWindowedLoading():
    split = {'train': 0.7, 'val': 0.15, 'test': 0.15}
    dataset = WhiteNoiseDataset(input_window_size=1,
                                input_hop=1,
                                output_window_size=1,
                                output_hop=1)
    data_loader, _, _ = prepare_data_loaders(dataset, split=split, batch_size=1, shuffle=False)
    for i, batch in enumerate(data_loader):
        input, target = Variable(batch[0]), Variable(batch[1])
        print(input)
        print(target)
        print('------')



#----------------------------------------------------------------------

#testWhiteNoise()
#testSine()
testWindowedLoading()





