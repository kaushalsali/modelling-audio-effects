import sys
import os
import gzip
import shutil
import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot as plt
import pickle
import numpy as np
import torch.utils.data as data
import torch.nn as nn
import argparse
sys.path.append("../network/")
from datasets import SineTestDataset, TestDataset
import networkutils
sys.path.append("../datautils/")
import ioutils


def plotWave(signals_list, title, save_folder):
    signals_list = np.array(signals_list)
    if np.max(signals_list[1][0]) > np.max(signals_list[0][0]):
        signals_list = np.flip(signals_list, axis=0)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_title(title)
    for each in signals_list:
        ax.plot(each[0], label=each[1])
    ax.legend()
    ax.grid()
    savePlot(fig, title, save_folder)
    plt.show()


def savePlot(fig, name, folder='', pickle_plot=True):
    loc = '../../runs/testrun/' + folder
    if not os.path.exists(loc):
        os.makedirs(loc)
    fig.savefig(os.path.join(loc, name + '.png'))
    if pickle_plot:
        pickle.dump(fig, gzip.open(os.path.join(loc, name + '.pklz'), 'wb'))

def getTestFiles():
    loc = '../../data/test/input/'
    if os.path.exists(os.path.join(loc, '.DS_Store')):
        os.remove(os.path.join(loc, '.DS_Store'))
    return sorted(os.listdir(loc))

def showTestFileIndices():
    loc = '../../data/test/input/'
    if os.path.exists(os.path.join(loc, '.DS_Store')):
        os.remove(os.path.join(loc, '.DS_Store'))
    files = sorted(os.listdir(loc))
    print('\nAvailable Test Files:')
    for i,each in enumerate(files):
        print(i,each)

parser = argparse.ArgumentParser(description='Generate Datasets')
parser.add_argument('--input-size', type=int, default=128)
parser.add_argument('--file-format', type=str, default='wav')
parser.add_argument('--file-indices', type=int, nargs='+', default=None)
parser.add_argument('--model', type=str, default='model')
parser.add_argument('--visualize', type=str, default='filter')
args = parser.parse_args()

if args.file_indices is None:
    args.file_indices = range(len(getTestFiles()))


def testWhitenoiseSingle(file_id=0):
    test_files = getTestFiles()
    save_folder = test_files[file_id][:-4]

    whitetest = TestDataset(file_id=file_id,
                            input_window_size=args.input_size,
                            input_hop=1,
                            output_window_size=1,
                            output_hop=1,
                            file_format=args.file_format)

    white_data_loader = data.DataLoader(whitetest, batch_size=32)
    model = networkutils.loadModel(args.model)
    mse_loss = nn.MSELoss()
    avg_mse, max_mse, avg_mae, max_mae, ip_cache, pred_cache, t_cache = networkutils.test(model=model,
                                                                                          data_loader=white_data_loader,
                                                                                          visualize=args.visualize,
                                                                                          epoch='testrun',
                                                                                          save_folder=save_folder)

    print("Windowed signal losses:")
    print("Avg MSE Loss: ", avg_mse)
    print("Avg MAE Loss: ", avg_mae)
    print()
    print("Max MSE Loss: ", max_mse)
    print("Max MAE Loss: ", max_mae)
    print()
    print("Max & Min Input: ", np.max(ip_cache.numpy()), np.min(ip_cache.numpy()))
    print("Max & Min Target: ", np.max(t_cache.numpy()), np.min(t_cache.numpy()))
    print("Max & Min Prediction: ", np.max(pred_cache.detach().numpy()), np.min(pred_cache.detach().numpy()))

    print("\nReconstructed signal losses:")
    print("MSE loss: ", np.average(np.power(pred_cache.detach().numpy() - t_cache.numpy(), 2)))
    print("MAE loss: ", np.average(np.abs(pred_cache.detach().numpy() - t_cache.numpy())))


    plotWave([[ip_cache.numpy(), 'Input'], [t_cache.numpy(), 'Target']], title='input & target', save_folder=save_folder)
    #plotWave([[ip_cache.numpy(), 'Input'], [pred_cache.detach().numpy(), 'Prediction']], title='input & prediction', save_folder=save_folder)
    plotWave([[t_cache.numpy(), 'Target'], [pred_cache.detach().numpy(), 'Prediction']], title='target & prediction', save_folder=save_folder)

    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    ax1.plot(np.power(pred_cache.detach().numpy() - t_cache.numpy(), 2))
    ax1.set_title('Squared error loss after reconstruction')
    ax1.grid()
    savePlot(fig1, 'MSE_loss_reconstructed_signal', folder=save_folder)
    plt.show()
    plt.close()

    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111)
    ax2.plot(np.abs(pred_cache.detach().numpy() - t_cache.numpy()))
    ax2.set_title('Absolute error loss after reconstruction')
    ax2.grid()
    savePlot(fig2, 'MAE_loss_reconstructed_signal', folder=save_folder)
    plt.show()
    plt.close()

    if not os.path.exists('../../runs/testrun/' + save_folder + '/prediction/'):
        os.makedirs('../../runs/testrun/' + save_folder + '/prediction/')

    ioutils.wavwrite('../../data/test/prediction/prediction_' + save_folder + '.wav', 44100, pred_cache.detach().numpy())
    ioutils.wavwrite('../../runs/testrun/' + save_folder + '/prediction/prediction_' + save_folder + '.wav', 44100, pred_cache.detach().numpy())

    # Log Results (Common file for all test samples)
    with open('../../runs/testrun/testrun.log', 'a') as f:
        f.write("\nFilename: " + save_folder + '\n\n')
        f.write("Windowed signal losses:" + '\n')
        f.write("Avg MSE Loss: " + str(avg_mse) + '\n')
        f.write("Avg MAE Loss: " + str(avg_mae) + '\n\n')

        f.write("Max MSE Loss: " + str(max_mse) + '\n')
        f.write("Max MAE Loss: " + str(max_mae) + '\n\n')

        f.write("Max & Min Input: " + str(np.max(ip_cache.numpy())) + " " + str(np.min(ip_cache.numpy())) + '\n')
        f.write("Max & Min Target: " + str(np.max(t_cache.numpy())) + " " + str(np.min(t_cache.numpy())) + '\n')
        f.write("Max & Min Prediction: " + str(np.max(pred_cache.detach().numpy())) + " " + str(np.min(pred_cache.detach().numpy())) + '\n')

        f.write("\nReconstructed signal losses:" + '\n')
        f.write("MSE loss: " + str(np.average(np.power(pred_cache.detach().numpy() - t_cache.numpy(), 2))) + '\n')
        f.write("MAE loss: " + str(np.average(np.abs(pred_cache.detach().numpy() - t_cache.numpy()))) + '\n')
        f.write("---------------------------------------\n")


def testWhitenoiseMultiple(indices):
    # Delete old prediction files
    if os.path.exists('../../data/test/prediction'):
        shutil.rmtree('../../data/test/prediction')
        os.makedirs('../../data/test/prediction')
    # Backup testrun and delete old testrun
    if os.path.exists('../../runs/testrun/'):
        if os.path.exists('../../other/testrun_old'):
            shutil.rmtree('../../other/testrun_old')
        shutil.move('../../runs/testrun/', '../../other/testrun_old')
        os.makedirs('../../runs/testrun/')

    test_files = getTestFiles()
    print('\nSelected Test Files:\n', indices)
    print('\n-------Testing-------')
    for i in indices:
        print('\nProcessing file', i, ':', test_files[i] + '\n')
        testWhitenoiseSingle(i)
        print()


#-----------------------------


showTestFileIndices()
#testWhitenoiseMultiple(range(len(getTestFiles())))
testWhitenoiseMultiple(args.file_indices)
