# Modelling Audio Effects using Neural Networks

This project aims to emulate audio effects using a multi layer perceptron. You can generate data and train and test it using various model configurations.

The audio effects that can be currently modelled successfully are waveshapers and filters.

